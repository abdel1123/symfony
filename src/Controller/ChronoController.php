<?php

namespace App\Controller;
use App\Repository\ChronoRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Chrono;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class ChronoController extends AbstractController
{
    #[Route('/chrono', name: 'app_chrono')]
    public function chronoStart(ChronoRepository $repo): Response
    {

        $chrono = new Chrono();

        $chrono->setStart(new \DateTime());//->format('Y-m-d H:i:s');
        $chrono->setStop(new \DateTime());
        $repo->save($chrono, true);
        $id = $chrono->getId();
        
        //var_dump(new \DateTime());

      
        return $this->render('chrono/index.html.twig', [
            'controller_name' => 'chrono'.$id,
            'texte' => $chrono->getstart()->format('Y-m-d H:i:s')
        ]);
    }

    #[Route('/stop-chrono', name: 'app_chronoStop')]
    public function chronoStop(ChronoRepository $repo, Request $req): Response
    {
        
        $id_chrono = $req->query->get('id');
        $chrono = $repo->find($id_chrono);
        $chrono->setStop(new \DateTime());
        var_dump($id_chrono);

      
        return $this->render('chrono/index.html.twig', [
            'texte1' => $chrono->getStop()->format('Y-m-d H:i:s')
        ]);
    }
}
